import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DelSearchForm extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DelSearchForm frame = new DelSearchForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public static void NewScreen7() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DelSearchForm frame = new DelSearchForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DelSearchForm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 447, 422);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSearchDeliveryPerson = new JLabel("Search Delivery Person");
		lblSearchDeliveryPerson.setFont(new Font("宋体", Font.PLAIN, 16));
		lblSearchDeliveryPerson.setBounds(121, 26, 244, 49);
		contentPane.add(lblSearchDeliveryPerson);
		
		JLabel label_1 = new JLabel("Id:");
		label_1.setFont(new Font("宋体", Font.PLAIN, 13));
		label_1.setBounds(82, 139, 81, 15);
		contentPane.add(label_1);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(173, 136, 96, 21);
		contentPane.add(textField);
		
		JLabel label_2 = new JLabel("Name:");
		label_2.setFont(new Font("宋体", Font.PLAIN, 13));
		label_2.setBounds(82, 187, 81, 15);
		contentPane.add(label_2);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(173, 184, 96, 21);
		contentPane.add(textField_1);
		
		JButton button = new JButton("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button.setFont(new Font("宋体", Font.PLAIN, 13));
		button.setBounds(40, 267, 115, 32);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Cancel");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
//				CustChoose nw=new CustChoose();
//				nw.NewScreen4();
			}
		});
		button_1.setFont(new Font("宋体", Font.PLAIN, 13));
		button_1.setBounds(232, 267, 115, 32);
		contentPane.add(button_1);
	}
}
