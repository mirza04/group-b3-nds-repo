import junit.framework.TestCase;

public class CustomerTest extends TestCase {

	// Test no. 1
	// Objective to test: To test LN int vs str compare
	// Input(s): UserName = "abc"
	// Expected Output: False
	public void testvalidateLastName001() {
		
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateLastName("Donoghue12345"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 2
	// Objective to test: To test LN Letters compare
	// Input(s): UserName = "Donoghue"
	// Expected Output: True
	public void testvalidateLastName002() {
	
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateLastName("Donoghue"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Test no. 1
	// Objective to test: To test LN int vs str compare
	// Input(s): UserName = "abc"
	// Expected Output: False
	public void testvalidateOtherNames001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateOtherNames("Donoghue12345"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 2
	// Objective to test: To test LN Letters compare
	// Input(s): UserName = "Donoghue"
	// Expected Output: True
	public void testvalidateOtherNames002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateOtherNames("Donoghue"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Date of birth tests

	public void testvalidateDOB001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateDOB("11-22-96"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	public void testvalidateDOB002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateDOB("112-22-9a"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	// Address tests
	public void testvalidateAddress001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateAddress("# Wilow Park 123"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	public void testvalidateAddress002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateAddress("Willow Park 123"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Region Id

	public void testvalidateRegionId001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateRegion("1"));
		} catch (Exception e) {
			fail("Exception not Expected");
		}

	}

	public void testvalidateRegionId002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateRegion(""));
		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 1
	// Objective to test: To test LN int vs str compare
	// Input(s): UserName = "abc"
	// Expected Output: False
	public void testvalidateDeliveryFrequency001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validateDeliveryFrequency("0"));

		} catch (Exception e) {
			fail("Exception Expected");
		}

	}

	// Test no. 2
	// Objective to test: To test LN Letters compare
	// Input(s): UserName = "Donoghue"
	// Expected Output: True
	public void testvalidateDeliveryFrequency002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validateDeliveryFrequency("1"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}

	public void testvalidatephoneNumber001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(false, testObject.validatePhoneNumber("dfcghi"));

		} catch (Exception e) {
			fail("Exception expected");
		}

	}

	public void testvalidatephoneNumber002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals(true, testObject.validatePhoneNumber("0987654321"));

		} catch (Exception e) {
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateCustomer001() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("ok", testObject.createCustomer("Fallon", "John", "22-12-18", "83 Auburn Heights, Athlone", "", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	
	public void testvalidatecreateCustomer002() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Last Name", testObject.createCustomer("123%$Fallon", "John", "22-12-18", "83 Auburn Heights, Athlone", "", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}

	}
	
	public void testvalidatecreateCustomer003() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Other Name", testObject.createCustomer("Fallon", "123%John$%", "22-12-18", "83 Auburn Heights, Athlone", "", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception expected");
		}
	}
	
	public void testvalidatecreateCustomer004() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect DOB", testObject.createCustomer("Fallon", "John", "testDOB", "83 Auburn Heights, Athlone", "", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
	
	public void testvalidatecreateCustomer005() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Address", testObject.createCustomer("Fallon", "John", "22-12-18", "/83 A%ub%urn$ Heig^hts, Athlone", "", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
	
	public void testvalidatecreateCustomer006() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Region", testObject.createCustomer("Fallon", "John", "22-12-18", "83 Auburn Heights, Athlone", "1", "Weekly", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}
	}
	
	public void testvalidatecreateCustomer007() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Delivery Freq", testObject.createCustomer("Fallon", "John", "22-12-18", "83 Auburn Heights, Athlone", "", "0", "0891234567"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
	public void testvalidatecreateCustomer008() {
		try {
			DatabaseConnector db = new DatabaseConnector();
			Customer testObject = new Customer(db);
			assertEquals("Incorrect Phone Number", testObject.createCustomer("Fallon", "John", "22-12-18", "83 Auburn Heights, Athlone", "", "Weekly", "testblabla"));

		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected");
		}

	}
}
